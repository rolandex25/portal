<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\Model;
use Faker\Generator as Faker;
use App\Models\Profesion;

$factory->define(Profesion::class, function (Faker $faker) {
    return [
        'nombre'=>$faker->firstNameMale(),
        'codigo'=>$faker->randomDigit()
    ];
});
