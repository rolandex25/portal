<?php

use Illuminate\Database\Seeder;
use App\Models\Profesion;

class ProfesionSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        
        Profesion::create([
            'nombre'=>'Abogado',
            'codigo'=>'12345'
        ]);
        Profesion::create([
            'nombre'=>'Informatico',
            'codigo'=>'3345'
        ]);
        Profesion::create([
            'nombre'=>'Auditor',
            'codigo'=>'1423'
        ]);

        factory(Profesion::class,5)->create();
    }
}
