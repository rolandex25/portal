<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/
//Route con dos puntos es patron FACADE (fachada)

Route::get('/', function () {
    return view('welcome');
});

//Ruta con parametro opcional
Route::get('/saludo/{nombre?}', function ($nombre=null) {
    if($nombre){
        return "Hola como estas $nombre";
    } else {
        return "Hola desconocido";
    }
});

Route::get('/usuarios', function () {
    return "Lista de usuarios";
});

Route::get('/usuario/{id_usuario}', function ($id_usuario) {
    return "Detalle del usuario".$id_usuario;
})->where('id_usuario','[0-9]+');


//----Profesiones
//listar
Route::get('profesion/index','ProfesionController@index')->name('profesion.index');

//mostrar formulario crear
Route::get('profesion/create','ProfesionController@create')->name('profesion.create');
Route::post('profesion/create/store','ProfesionController@createStore');

//mostrar formulario editar
Route::get('profesion/edit/{id_profesion}','ProfesionController@edit')->name('profesion.edit');
Route::put('profesion/edit/store','ProfesionController@editStore');
//Tambien puede usarse post. Pero para modificar se suele usar put

//Borrado directo
Route::get('profesion/delete/{id_profesion}','ProfesionController@delete')->name('profesion.delete');

Route::get('profesion/reporte','ProfesionController@reporte')->name('profesion.reporte');

Route::get('profesion/reporteexcel','ProfesionController@reporteExcel')->name('profesion.reporteexcel');
