+--------+----------+---------------------------------+------------------+------------------------------------------------------+--------------+
| Domain | Method   | URI                             | Name             | Action                                               | Middleware   |
+--------+----------+---------------------------------+------------------+------------------------------------------------------+--------------+
|        | GET|HEAD | /                               |                  | Closure                                              | web          |
|        | GET|HEAD | api/user                        |                  | Closure                                              | api,auth:api |
|        | GET|HEAD | profesion/create                | profesion.create | App\Http\Controllers\ProfesionController@create      | web          |
|        | POST     | profesion/create/store          |                  | App\Http\Controllers\ProfesionController@createStore | web          |
|        | GET|HEAD | profesion/delete/{id_profesion} | profesion.delete | App\Http\Controllers\ProfesionController@delete      | web          |
|        | PUT      | profesion/edit/store            |                  | App\Http\Controllers\ProfesionController@editStore   | web          |
|        | GET|HEAD | profesion/edit/{id_profesion}   | profesion.edit   | App\Http\Controllers\ProfesionController@edit        | web          |
|        | GET|HEAD | profesion/index                 | profesion.index  | App\Http\Controllers\ProfesionController@index       | web          |
|        | GET|HEAD | saludo/{nombre?}                |                  | Closure                                              | web          |
|        | GET|HEAD | usuario/{id_usuario}            |                  | Closure                                              | web          |
|        | GET|HEAD | usuarios                        |                  | Closure                                              | web          |
+--------+----------+---------------------------------+------------------+------------------------------------------------------+--------------+
